/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BackEnd.Controller;

import BackEnd.Model.Customer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MyPC
 */
public class CustomerManagementTest {
    
    public CustomerManagementTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }



    /**
     * Test of checkDuplicate method, of class CustomerManagement.
     */
    @Test
    public void testCheckDuplicate() throws Exception {
        System.out.println("checkDuplicate");
        String CMT = "123456789";
        boolean expResult = true;
        boolean result = CustomerManagement.checkDuplicate(CMT);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }


    /**
     * Test of getCustomer method, of class CustomerManagement.
     */
    @Test
    public void testGetCustomerByCMT() throws Exception {
        System.out.println("getCustomer");
        String CMT = "123456789";
        Customer expResult = null;
        Customer result = CustomerManagement.getCustomerByCMT(CMT);
        assertEquals("Dao Trong Nghia", result.getName());

    }
    
}
