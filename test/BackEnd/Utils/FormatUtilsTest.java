/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BackEnd.Utils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MyPC
 */
public class FormatUtilsTest {
    
    public FormatUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  

    /**
     * Test of isPlate method, of class FormatUtils.
     */
    @Test
    public void testIsPlate() {
        System.out.println("isPlate");
        String s = "12F412345";
        boolean expResult = true;
        boolean result = FormatUtils.isPlate(s);
        assertEquals(expResult, result);
         s = "12FF1234";
        expResult = true;
         result = FormatUtils.isPlate(s);
        assertEquals(expResult, result);
           s = "123";
        expResult = false;
         result = FormatUtils.isPlate(s);
        assertEquals(expResult, result);
           s = "29K56789";
        expResult = true;
         result = FormatUtils.isPlate(s);
        assertEquals(expResult, result);

    }
    
}
