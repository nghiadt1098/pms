
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BackEnd.Utils;

//~--- non-JDK imports --------------------------------------------------------

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static BackEnd.Utils.HashDigest.getMd5;

import static org.junit.Assert.*;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Image;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.net.URL;

import java.security.NoSuchAlgorithmException;

import javax.imageio.ImageIO;

import javax.swing.ImageIcon;

/**
 *
 * @author MyPC
 */
public class ImageUtilsTest {
    public ImageUtilsTest() {}

    @BeforeClass
    public static void setUpClass() {}

    @AfterClass
    public static void tearDownClass() {}

    @Before
    public void setUp() {}

    @After
    public void tearDown() {}

    /**
     * Test of encodeToString method, of class ImageUtils.
     */
    @Test
    public void testEncodeToString() throws IOException, NoSuchAlgorithmException, Exception {
        System.out.println("encode");

        BufferedImage image     = null;
        String        type      = "";
        String        expResult = "c37f2227ad488874b25cef290b8b050e";
        String        result ="";
        try{
           result    = getMd5(ImageUtils.encoder(ImageIO.read(new File("a.png"))));
        } catch(NoSuchAlgorithmException ex){
            System.out.println("MD5 not found");
        }
       

        assertEquals(expResult, result);

        // TODO review the generated test code and remove the default call to fail.
    }
    
     /**
     * Test of encodeToString method, of class ImageUtils.
     */
    @Test
    public void testEncodeToPath() throws IOException, NoSuchAlgorithmException, Exception {
        System.out.println("encoder with path");

        BufferedImage image     = null;

        String expResult="dd48ba2a8ec6e79b099b84e4ac7565c9";
        String        result    = getMd5(ImageUtils.encoder("a.png"));
        System.out.println(result);
        assertEquals(expResult, result);

        // TODO review the generated test code and remove the default call to fail.
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
