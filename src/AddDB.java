import java.awt.image.BufferedImage;

import java.io.File;

import java.util.Date;
import java.util.Scanner;

import BackEnd.Controller.CustomerManagement;
import BackEnd.Controller.LogManagement;
import BackEnd.Controller.PlateManagement;

import BackEnd.Model.Customer;
import BackEnd.Model.Log;
import BackEnd.Model.Plate;

import BackEnd.Utils.ImageUtils;

public class AddDB {
    public static void addCustomer() {
        try {
            Scanner sc = new Scanner(new File("Data\\se1210.txt"));

            while (sc.hasNextLine()) {
                String        s       = sc.nextLine();
                String[]      arr     = s.split("[|]");
                String        name    = arr[2];
                boolean       sex     = true;
                Date          dob     = ranDate();
                String        CMT     = ranCMT();
                BufferedImage image   = ImageUtils.readFromFile("Data\\Avatar\\" + arr[0] + ".png");
                String        phone   = ranPhone();
                String        address = arr[1];
                Customer      c       = new Customer(name, sex, dob, CMT, image, phone, address);

                CustomerManagement.addCustomer(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addLog() {
        try {
            String[] arr = { "29K56789", "49S48838", "29T82843", "77H77777" };

            for (String s : arr) {
                Date timeLog = new Date();

                Thread.sleep(100);

                boolean       status = true;
                BufferedImage imgLog = ImageUtils.readFromFile("Data\\Plate\\" + s + ".png");
                Log           l      = new Log(s, timeLog, status, imgLog);

                LogManagement.addLog(l);
            }

            for (String s : arr) {
                Date timeLog = new Date();

                Thread.sleep(100);

                boolean       status = false;
                BufferedImage imgLog = ImageUtils.readFromFile("Data\\Plate\\" + s + ".png");
                Log           l      = new Log(s, timeLog, status, imgLog);

                LogManagement.addLog(l);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addPlate() {
        try {
            for (int i = 1; i <= 25; i++) {
                String s = "29N100";

                if (i < 10) {
                    s += "0" + i;
                } else {
                    s += i;
                }

                Plate p = new Plate(s, i);

                PlateManagement.addPlate(p);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        updateNghia();
    }

    public static String ranCMT() {
        String s = "";

        for (int i = 0; i < 9; i++) {
            s += (int) Math.floor(Math.random() * 10);
        }

        return s;
    }

    public static Date ranDate() {
        long x = Math.round(Math.random() * 10e11);

        return new Date(x);
    }

    public static String ranPhone() {
        String[] arr = { "016", "012", "09" };
        int      x   = (int) Math.floor(Math.random() * 3);
        String   s   = arr[x];

        for (int i = 0; i < 8; i++) {
            s += (int) Math.floor(Math.random() * 10);
        }

        return s;
    }

    public static void updateNghia() {
        try {
            String s  = "29K56789";
            int    id = 9;
            Plate  p  = new Plate(s, id);

            PlateManagement.addPlate(p);

            Date          timeLog = new Date();
            Thread.sleep(100);
            boolean       status  = true;
            BufferedImage imgLog  = ImageUtils.readFromFile("Data\\Plate\\" + s + ".png");
            Log           l       = new Log(s, timeLog, status, imgLog);

            LogManagement.addLog(l);
            timeLog = new Date();
            status  = false;
            l       = new Log(s, timeLog, status, imgLog);
            LogManagement.addLog(l);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
