
import FrontEnd.MainFrame.MainFrame;
import FrontEnd.ManageFrame.ManageFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MyPC
 */
public class MainAppController {
    @FXML
    private Button btnManage;
    
    @FXML 
    private Button btnSystem;
    @FXML 
    public void mouseCursorIn(){
        MainApplication.scene.setCursor(Cursor.HAND);
    }
        @FXML 
    public void mouseCursorOut(){
        MainApplication.scene.setCursor(Cursor.DEFAULT);
    }
    @FXML
    public void startManager() {
       MainApplication.getPrimaryStage().close();
        /* Set the Nimbus look and feel */

        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">

        /*
         *  If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());

                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
                                               ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
                                               ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
                                               ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null,
                                               ex);
        }

        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame=new ManageFrame();
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setTitle("Parking Manager");
                frame.setVisible(true);
            }
        });
        
        
    }
    @FXML
    public void startParking() {
        MainApplication.getPrimaryStage().close();
        JFrame frame = new MainFrame();

        
    }
    
}
