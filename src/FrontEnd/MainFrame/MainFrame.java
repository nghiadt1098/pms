
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Controller.ParkingManagement;

import FrontEnd.MainFrame.LogTable.LogTable;
import FrontEnd.MainFrame.Panel.AvatarPanel;
import FrontEnd.MainFrame.Panel.CropImagePanel;
import FrontEnd.MainFrame.Panel.ImagePanel;
import FrontEnd.MainFrame.Panel.PlateInfomationPanel;
import FrontEnd.MainFrame.Panel.QRInfoPanel;
import FrontEnd.MainFrame.Panel.TabScanPanel;

import Resources.ConstString;

//~--- JDK imports ------------------------------------------------------------

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;

import static java.lang.Thread.sleep;

/**
 *
 * @author MyPC
 */
public class MainFrame extends JFrame implements Runnable {
    final static String             LOOKANDFEEL = "System";
    final static String             THEME       = "DefaultMetal";
    public static ParkingManagement pms;
    public static ImagePanel        adPanel;
    private TabScanPanel            tabScanPanel;
    private QRInfoPanel             qrInfoPanel;
    private LogTable                logTable;
    private CropImagePanel          imagePanel;
    private PlateInfomationPanel    plateInfomationPanel;

    public MainFrame() {
        super(ConstString.mainFrameTitle);
        pms = new ParkingManagement();

        // initLookAndFeel();
        // GridBagLayout gidBL=;
        GridLayout c = new GridLayout(0, 3);

        qrInfoPanel          = new QRInfoPanel();
        plateInfomationPanel = new PlateInfomationPanel();
        imagePanel           = new CropImagePanel(plateInfomationPanel);
        plateInfomationPanel.setCropImagePanel(imagePanel);
        plateInfomationPanel.setqRInfoPanel(qrInfoPanel);
        tabScanPanel = new TabScanPanel(qrInfoPanel, plateInfomationPanel, imagePanel);
        logTable     = new LogTable();
        this.setLayout(c);
        this.initFirstRow();
        this.init2ndRow();

        // this.init3rdRow(c);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
        this.run();
    }
    private int count=0;
    @Override
    public void run() {
        while (true) {

                try {
                    
                    MainFrame.adPanel.setImage(ImageIO.read(new File(String.valueOf(count%3)+".jpg")));
                    count=count+1;
                    sleep(3000);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            
        }
    }

    private void initFirstRow() {
        adPanel = new ImagePanel();
        this.getContentPane().add(adPanel);

        // tabbedPane.setSize(new Dimension(300,200));
        this.getContentPane().add(tabScanPanel);
        logTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
        logTable.setFillsViewportHeight(true);
        logTable.updateInfo();
        this.getContentPane().add(new JScrollPane(logTable));
        plateInfomationPanel.setLogTable(logTable);
    }

    private void init2ndRow() {
        this.getContentPane().add(plateInfomationPanel);
        imagePanel.setBackground(Color.black);
        this.getContentPane().add(imagePanel);
        this.getContentPane().add(qrInfoPanel);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
