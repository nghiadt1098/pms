
/*-
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.LogTable;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Model.*;
import BackEnd.Utils.FormatUtils;

import FrontEnd.MainFrame.MainFrame;
import java.awt.Font;

//~--- JDK imports ------------------------------------------------------------

import java.sql.Array;
import java.sql.Timestamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author MyPC
 */
public class LogTable extends JTable {
    public LogTable() {
        super();    
        this.setFont(new Font("SansSerif", Font.PLAIN, 14));
        this.setModel(new TableModel());

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date             parsedDate = dateFormat.parse("2018-10-09 10:10:10");
        } catch (ParseException ex) {
            Logger.getLogger(LogTable.class.getName()).log(Level.SEVERE, null, ex);
        }

        String header[] = { "Plate license", "Time", "STATUS" };;

        for (int i = 0; i < this.getColumnCount(); i++) {
            TableColumn column1 = this.getTableHeader().getColumnModel().getColumn(i);

            column1.setHeaderValue(header[i]);
        }

        this.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        this.doLayout();
    }

    public void updateInfo() {
        TableModel model = (TableModel) this.getModel();

        try {
            model.setList(MainFrame.pms.getLogs(20));
            model.fireTableDataChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class TableModel extends AbstractTableModel {
        String[]       cName = { "Plate license", "Time", "STATUS" };
        ArrayList<Log> list  = new ArrayList<>();

        @Override
        public int getRowCount() {
            return list.size();
        }

        @Override
        public int getColumnCount() {
            return cName.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
            case 0 :
                return FormatUtils.formatPlate(list.get(rowIndex).getPlateNumber());

            case 1 :
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(list.get(rowIndex).getTimeLog());

            case 2 :
                return list.get(rowIndex).isStatus()
                       ? "IN"
                       : "OUT";
            }

            return null;
        }

        public ArrayList<Log> getList() {
            return list;
        }

        public void setList(ArrayList<Log> list) {
            this.list = list;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;    // To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public String getColumnName(int column) {
            return cName[column];    // To change body of generated methods, choose Tools | Templates.
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
