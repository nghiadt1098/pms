
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- non-JDK imports --------------------------------------------------------

import FrontEnd.MainFrame.MainFrame;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author MyPC
 */
public class QRInfoPanel extends JPanel {
    private ImagePanel qrImage, capQRImage;
    private JTextField txfPlate, txfTimeReg, txfTimeExpire, txfStatus;
    private JLabel     txtPlate, txtTimeReg, txtTimeExpire, txtStatus;
    private String qrCode;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
    public void clearAll(){
        txfPlate.setText("");
        txfTimeReg.setText("");
        txfTimeExpire.setText("");
        txfStatus.setText("");
        qrImage.clearImg();
        capQRImage.clearImg();
        qrCode="";
    }
    public QRInfoPanel() {
        super();

        TitledBorder  b      = BorderFactory.createTitledBorder("QR Info");
        GridBagLayout layout = new GridBagLayout();

        this.setLayout(layout);
        this.setBorder(b);

        GridBagConstraints c = new GridBagConstraints();

        /** ***************************************************** */
        qrImage = new ImagePanel();
        qrImage.setBackground(Color.green);
        c.fill    = GridBagConstraints.BOTH;
        c.weighty = 1;
        c.weightx = 1;
        c.gridx   = 0;
        c.gridy   = 0;
        this.add(qrImage, c);

        /** ***************************************************** */
        capQRImage = new ImagePanel();
        capQRImage.setBackground(Color.red);
        c.fill    = GridBagConstraints.BOTH;
        c.weighty = 1;
        c.weightx = 1;
        c.gridx   = 1;
        c.gridy   = 0;
        this.add(capQRImage, c);
        c.fill   = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.CENTER;

        /** ***************************************************** */
        txtPlate  = new JLabel("Plate number:");
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 0;
        c.gridy   = 1;
        this.add(txtPlate, c);

        /** ***************************************************** */
        txfPlate = new JTextField();
        txfPlate.setEditable(false);
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 1;
        c.gridy   = 1;
        this.add(txfPlate, c);

        /** ***************************************************** */
        txtTimeReg = new JLabel("Registered at:");
        c.weighty  = 0.1;
        c.weightx  = 1;
        c.gridx    = 0;
        c.gridy    = 2;
        this.add(txtTimeReg, c);

        /** ***************************************************** */
        txfTimeReg = new JTextField();
        txfTimeReg.setEditable(false);
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 1;
        c.gridy   = 2;
        this.add(txfTimeReg, c);

        /** ***************************************************** */
        txtTimeExpire = new JLabel("Expire at:");
        c.weighty     = 0.1;
        c.weightx     = 1;
        c.gridx       = 0;
        c.gridy       = 3;
        this.add(txtTimeExpire, c);

        /** ***************************************************** */
        txfTimeExpire = new JTextField();
        txfTimeExpire.setEditable(false);
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 1;
        c.gridy   = 3;
        this.add(txfTimeExpire, c);

        /** ***************************************************** */
        txtStatus = new JLabel("Status");
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 0;
        c.gridy   = 4;
        this.add(txtStatus, c);

        /** ***************************************************** */
        txfStatus = new JTextField();
        txfStatus.setEditable(false);
        c.weighty = 0.1;
        c.weightx = 1;
        c.gridx   = 1;
        c.gridy   = 4;
        this.add(txfStatus, c);

        /** ***************************************************** */
    }

    public void setQrImage(BufferedImage qrImage) {
        this.qrImage.setImage(qrImage);
    }

    public void setCapQRImage(BufferedImage capQRImage) {
        this.capQRImage.setImage(capQRImage);
    }

    public void setTxtPlate(String txtPlate) {
        this.txfPlate.setText(txtPlate);
    }

    public void setTxtTimeReg(String txtTimeReg) {
        this.txfTimeReg.setText(txtTimeReg);
    }

    public void setTxtTimeExpire(String txtTimeExpire) {
        this.txfTimeExpire.setText(txtTimeExpire);
    }

    public void setTxtStatus(Color c, String txtStatus) {
        this.txfStatus.setForeground(c);
        this.txfStatus.setText(txtStatus);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
