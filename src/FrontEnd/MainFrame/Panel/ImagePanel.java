
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 *
 * @author MyPC
 */
public class ImagePanel extends JPanel {
    private Image img;

    public ImagePanel() {
        super();
    }

    public void setImage(Image image) {
        this.img = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
        System.out.println(this.getWidth() + " " + this.getHeight());
        this.repaint();
    }
    public void clearImg(){
        this.img= new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        
         this.repaint();
    }
    public Image getImg() {
        return img;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, null);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
