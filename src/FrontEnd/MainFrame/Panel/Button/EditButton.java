
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel.Button;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Utils.ImageUtils;

//~--- JDK imports ------------------------------------------------------------

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.IOException;

import java.net.URL;

import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;

/**
 *
 * @author MyPC
 */
public class EditButton extends JButton implements ActionListener {
    private JTextField jtf;
    private ImageIcon  edit;
    private ImageIcon  ok;

    public EditButton(JTextField jtf) {
        super();
        this.jtf = jtf;
        this.addActionListener(this);
        this.setSize(30, 30);

        // this.setContentAreaFilled(false);
        try {
            ok   = ImageUtils.getScaledIco(System.class.getResource("/Resources/Icon/icons8_Checkmark_25px.png"), 25,
                                           25);
            edit = ImageUtils.getScaledIco(System.class.getResource("/Resources/Icon/icons8_Edit_File_25px_2.png"), 25,
                                           25);
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setIcon(edit);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        jtf.setEditable(jtf.isEditable() ^ true);

        if (jtf.isEditable()) {
            this.setIcon(ok);
        } else {
            this.setIcon(edit);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
