
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import javax.swing.JPanel;

/**
 *
 * @author MyPC
 */
public class AvatarPanel extends JPanel {
    private Image img;

    public AvatarPanel() {
        super();

        try {
            this.img = ImageIO.read(new File("avatar.png"));
        } catch (IOException ex) {

            // handle exception...
        }
    }

    public AvatarPanel(Image image) {
        super();
        this.img = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
    }

    public void update() {
        this.setPreferredSize(new Dimension(100, 100));
        this.setSize(this.getPreferredSize());
        this.img = this.img.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.img, 0, 0, this);    // see javadoc for more info on the parameters
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
