
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Controller.CardManagement;
import BackEnd.Controller.LogManagement;
import BackEnd.Controller.ParkingManagement;
import BackEnd.Controller.PlateManagement;

import BackEnd.Model.Card;
import BackEnd.Model.Log;
import BackEnd.Model.Plate;

import BackEnd.Utils.ANPRUtils;
import BackEnd.Utils.FormatUtils;
import BackEnd.Utils.QRUtils;
import BackEnd.Utils.Status;

import FrontEnd.MainFrame.MainFrame;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

import static FrontEnd.MainFrame.MainFrame.adPanel;

//~--- JDK imports ------------------------------------------------------------

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.File;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import javax.swing.JButton;

/**
 *
 * @author MyPC
 */
public class ScanPanel extends WebcamPanel implements ActionListener {
    public final static int      QRMODE    = 1;
    public final static int      PLATEMODE = 0;
    private QRPainter            qr        = new QRPainter();
    private DefaultPainter       de        = new DefaultPainter();
    private int                  mode;
    private Webcam               wc;
    private PlateInfomationPanel plateInfomationPanel;
    private TabScanPanel         tabScanPanel;
    private QRInfoPanel          qrInfoPanel;
    private JButton              btnScan;
    private ImagePanel           plateImage;
    private Status               result;

    public ScanPanel(Webcam webcam) {
        super(webcam);
    }

    public ScanPanel(Webcam webcam, Dimension size, boolean start) {
        super(webcam, size, start);
    }

    public ScanPanel(Webcam webcam, Dimension size, boolean start, ImageSupplier supplier) {
        super(webcam, size, start, supplier);
    }

    public ScanPanel(Webcam webcam, boolean start, PlateInfomationPanel p, QRInfoPanel q, ImagePanel ip,
                     TabScanPanel tabpanel) {
        super(webcam, start);
        this.wc                   = webcam;
        this.plateInfomationPanel = p;
        this.qrInfoPanel          = q;
        this.plateImage           = ip;
        btnScan                   = new JButton("Scan");
        btnScan.addActionListener(this);
        this.tabScanPanel = tabpanel;
        this.add(btnScan);
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void update() {
        switch (mode) {
        case ScanPanel.QRMODE :
            this.setPainter(qr);

            // this.
            // this.repaint();
            break;

        case ScanPanel.PLATEMODE :
            this.setPainter(de);

            break;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (this.getMode() == ScanPanel.PLATEMODE) {
                BufferedImage image = wc.getImage();

                for (String str : ANPRUtils.getANPRfromImage(image)) {
                    if (FormatUtils.isPlate(str.trim())) {
                        plateInfomationPanel.setTxtPlate(new Plate(str.trim()));
                        plateImage.setImage(image);

                        Date cur = new Date();

                        plateInfomationPanel.setTxtTimeIn((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(cur));
                        System.out.println(str);
                        tabScanPanel.setSelectedIndex(1);
                    }
                }
            } else {
                BufferedImage img = wc.getImage();
                String        res = QRUtils.getQRFromImage(wc.getImage());

                qrInfoPanel.setQrCode(res);
                System.out.println(res);
                result = MainFrame.pms.process(res, plateInfomationPanel.getPlate().getPlateNumber().trim());
                plateInfomationPanel.setTxtStatus(result);
                plateInfomationPanel.setCode(res);
                plateInfomationPanel.setImg(img);
                qrInfoPanel.setCapQRImage(img);
                qrInfoPanel.setQrImage(QRUtils.encodeQR(res));

                if (result == Status.NEWCARD) {
                    qrInfoPanel.setTxtPlate("NOT INFO");
                    qrInfoPanel.setTxtStatus(Color.BLUE, "NOT INFO");
                    qrInfoPanel.setTxtTimeExpire("NOT INFO");
                    qrInfoPanel.setTxtTimeReg("NOT INFO");
                    plateInfomationPanel.setButtonEnable(true);

                    return;
                }

                plateInfomationPanel.setButtonEnable(true);

                Card c = MainFrame.pms.getCardFromQR(res);

                System.out.println(c);
                qrInfoPanel.setTxtPlate(FormatUtils.formatPlate(c.getPlateNumber()));

                boolean status = LogManagement.getPlateStatus(c.getPlateNumber());

                System.out.println(c.getExpireDate());
                System.out.println(c.getRegisterDate());

                if (c.getRegisterDate() != null) {
                    qrInfoPanel.setTxtTimeReg(c.getRegisterDate().toString());
                }

                if (c.getExpireDate() != null) {
                    qrInfoPanel.setTxtTimeExpire(c.getExpireDate().toString());

                    Date date = new Date();

                    if (c.getExpireDate().before(date)) {
                        qrInfoPanel.setTxtStatus(Color.RED, "Expired.");
                    } else {
                        qrInfoPanel.setTxtStatus(Color.GREEN, "Not Expired yet.");
                    }
                }

                Log log = MainFrame.pms.getLatestLog();

                if (log.isStatus()) {
                    plateInfomationPanel.setTxtTimeOut(
                        (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(log.getTimeLog()));

                    // lpateImage.setImage(log.getImgLog());
                }

                tabScanPanel.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public class QRPainter extends DefaultPainter {
        private double pos = 0;

        public QRPainter() {
            super();
        }

        @Override
        public void paintPanel(WebcamPanel owner, Graphics2D g2) {
            super.paintPanel(owner, g2);
        }

        @Override
        public void paintImage(WebcamPanel owner, BufferedImage image, Graphics2D g2) {
            super.paintImage(owner, image, g2);
            g2.setColor(Color.red);
            g2.setStroke(new BasicStroke(2));
            pos += 0.15;
            g2.drawLine(owner.getWidth() / 8,
                        Math.round((float) (owner.getHeight() / 2 + owner.getHeight() / 3 * Math.cos(pos))),
                        7 * owner.getWidth() / 8,
                        (int) Math.round(owner.getHeight() / 2 + owner.getHeight() / 3 * Math.cos(pos)));
            g2.setColor(Color.GREEN);
            g2.drawLine(owner.getWidth() / 8,
                        Math.round((float) (owner.getHeight() / 2 + owner.getHeight() / 3 * Math.cos(pos + Math.PI))),
                        7 * owner.getWidth() / 8,
                        (int) Math.round(owner.getHeight() / 2 + owner.getHeight() / 3 * Math.cos(pos + Math.PI)));
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
