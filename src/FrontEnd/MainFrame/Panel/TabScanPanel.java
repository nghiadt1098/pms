
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Utils.ANPRUtils;
import BackEnd.Utils.QRUtils;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author MyPC
 */
public class TabScanPanel extends JTabbedPane {

    public TabScanPanel(QRInfoPanel q,PlateInfomationPanel p,CropImagePanel ip) {
        Webcam  wc     = Webcam.getDefault();
        JButton button = new JButton("Scan");

        wc.setViewSize(new Dimension(640, 480));
        
        ScanPanel wcPanel = new ScanPanel(wc, true ,p,q,ip,this);

        wcPanel.setMirrored(true);
        wcPanel.setDrawMode(WebcamPanel.DrawMode.FILL);
        wcPanel.setMode(ScanPanel.PLATEMODE);
        wcPanel.update();
        wcPanel.setSize(new Dimension(200, 100));
        this.addTab("Plate Scanner", null, wcPanel, "Scan license plate");
        this.addTab("QR Scanner", null, null, "Scan QR Code");
        
        this.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                wcPanel.setMode(wcPanel.getMode() ^ 1);
                wcPanel.update();
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
