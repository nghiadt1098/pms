
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package FrontEnd.MainFrame.Panel;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Model.Log;
import BackEnd.Model.Plate;

import BackEnd.Utils.FormatUtils;
import BackEnd.Utils.ImageUtils;
import BackEnd.Utils.Status;

import FrontEnd.MainFrame.LogTable.LogTable;
import FrontEnd.MainFrame.MainFrame;
import FrontEnd.MainFrame.Panel.Button.EditButton;

import com.github.sarxos.webcam.Webcam;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.FileInputStream;
import java.io.InputStream;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 *
 * @author MyPC
 */
public class PlateInfomationPanel extends JPanel {
    private JLabel         txtPlate, txtTimeIn, txtTimeOut, txtStatus, txtImage;
    private QRInfoPanel    qRInfoPanel;
    private Webcam         wc;
    private JTextField     txfPlate, txfTimeIn, txfTimeOut, txfStatus;
    private String         code;
    private JButton        button;
    private Plate          plate;
    private Status         status;
    private LogTable       logTable;
    private BufferedImage  img;
    private CropImagePanel cropImagePanel;

    public PlateInfomationPanel() {
        super();

        TitledBorder b = BorderFactory.createTitledBorder("Plate Infomation");

        wc = Webcam.getDefault();
        b.setTitleFont(new Font("Sans Serif", Font.ITALIC | Font.BOLD, 15));
        this.setBorder(b);

        // this.setPreferredSize(this.getMaximumSize());
        this.setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        gbc.weighty = 1;
        gbc.insets  = new Insets(5, 5, 5, 5);
        gbc.fill    = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.gridx   = 0;
        gbc.gridy   = 0;
        txtPlate    = new JLabel("Plate license:");
        txtPlate.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txtPlate, gbc);
        gbc.weightx = 9;
        gbc.gridx   = 1;
        gbc.gridy   = 0;
        txfPlate    = new JTextField("");
        txfPlate.setEditable(false);
        txfPlate.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txfPlate, gbc);
        gbc.gridx   = 2;
        gbc.gridy   = 0;
        gbc.weightx = 0.005;
        this.add(new EditButton(txfPlate), gbc);
        gbc.gridwidth = 2;
        gbc.insets    = new Insets(5, 5, 5, 5);
        gbc.fill      = GridBagConstraints.HORIZONTAL;
        gbc.weightx   = 1;
        gbc.gridx     = 0;
        gbc.gridy     = 1;
        txtTimeIn     = new JLabel("Time in:");
        txtTimeIn.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txtTimeIn, gbc);
        gbc.weightx = 9;
        gbc.gridx   = 1;
        gbc.gridy   = 1;
        txfTimeIn   = new JTextField("");
        txfTimeIn.setEditable(false);
        txfTimeIn.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txfTimeIn, gbc);
        gbc.insets  = new Insets(5, 5, 5, 5);
        gbc.fill    = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.gridx   = 0;
        gbc.gridy   = 2;
        txtTimeOut  = new JLabel("Time out:");
        txtTimeOut.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txtTimeOut, gbc);
        gbc.weightx = 9;
        gbc.gridx   = 1;
        gbc.gridy   = 2;
        txfTimeOut  = new JTextField("");
        txfTimeOut.setEditable(false);
        txfTimeOut.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txfTimeOut, gbc);
        gbc.insets  = new Insets(5, 5, 5, 5);
        gbc.fill    = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1;
        gbc.gridx   = 0;
        gbc.gridy   = 3;
        txtStatus   = new JLabel("Status:");
        txtStatus.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txtStatus, gbc);
        gbc.weightx = 9;
        gbc.gridx   = 1;
        gbc.gridy   = 3;
        txfStatus   = new JTextField("");
        txfStatus.setEditable(false);
        txfStatus.setFont(new Font("Sans Serif", Font.PLAIN, 18));
        this.add(txfStatus, gbc);
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill   = GridBagConstraints.NORTHWEST;
        gbc.anchor = GridBagConstraints.FIRST_LINE_START;
        button     = new JButton("Confirm");
        button.setEnabled(false);
        button.addActionListener(new ConfirmAction());
        gbc.weighty = 5;
        gbc.weightx = 1;
        gbc.gridx   = 0;
        gbc.gridy   = 4;
        this.add(button, gbc);
        gbc.weighty   = 5;
        gbc.weightx   = 1;
        gbc.gridx     = 1;
        gbc.gridwidth = 2;
        gbc.gridy     = 4;
        gbc.fill      = GridBagConstraints.BOTH;
    }

    public void setButtonEnable(boolean b) {
        button.setEnabled(b);
    }

    public void setLogTable(LogTable logTable) {
        this.logTable = logTable;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setqRInfoPanel(QRInfoPanel qRInfoPanel) {
        this.qRInfoPanel = qRInfoPanel;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public void setCropImagePanel(CropImagePanel cropImagePanel) {
        this.cropImagePanel = cropImagePanel;
    }

    public void setTxtStatus(Status status) {
        switch (status) {
        case MATCH :
            txfStatus.setForeground(Color.GREEN);
            txfStatus.setText("PLATE MATCH");

            break;

        case NOTMATCH :
            txfStatus.setForeground(Color.RED);
            txfStatus.setText("PLATE NOT MATCH");

            break;

        case NEWCARD :
            txfStatus.setForeground(Color.BLUE);
            txfStatus.setText("NEW CARD ENTRY");

            break;
        }
    }

    public void setTxtPlate(Plate plate) {
        this.plate = plate;
        this.txfPlate.setText(FormatUtils.formatPlate(plate.getPlateNumber()));
    }

    public Plate getPlate() {
        return this.plate;
    }

    public void setTxtTimeIn(String txtTimeIn) {
        this.txfTimeIn.setText(txtTimeIn);
    }

    public void setTxtTimeOut(String txtTimeOut) {
        this.txfTimeOut.setText(txtTimeOut);
    }

    class ConfirmAction implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                img = ImageUtils.toBufferedImage(cropImagePanel.getImg());
                MainFrame.pms.confirm(code, plate.getPlateNumber(), img);
                JOptionPane.showMessageDialog(null, "Confirm success", "Success", JOptionPane.INFORMATION_MESSAGE);
                plate = null;
                code  = null;
                txfStatus.setText("");
                txfPlate.setText("");
                txfTimeIn.setText("");
                txfTimeOut.setText("");
                cropImagePanel.clearImg();
                qRInfoPanel.clearAll();
                logTable.updateInfo();
                button.setEnabled(false);
            } catch (Exception ex) {
                Logger.getLogger(PlateInfomationPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
