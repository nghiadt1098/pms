/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEnd.ManageFrame;

import BackEnd.Controller.CustomerManagement;
import BackEnd.Controller.PlateManagement;
import BackEnd.Model.Customer;
import BackEnd.Model.Plate;
import BackEnd.Utils.FormatUtils;
import java.awt.Event;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author quanbui26111998
 */
public class SearchCustomerDialog extends javax.swing.JDialog {

    /**
     * Creates new form SearchCustomerDialog
     */
    private String oldCMND;
    private DefaultTableModel tableModel;
    private BufferedImage imageAvatar=null;
    private ManageFrame parent;
    public SearchCustomerDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        this.parent=(ManageFrame) parent;
        initComponents();

        jTablePlate.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "No.", "Plate Number"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        tableModel = (DefaultTableModel) jTablePlate.getModel();
        jPopupMenuTable.add(jMenuItemDelete);
        jTablePlate.setComponentPopupMenu(jPopupMenuTable);
        Customer cus = ((ManageFrame) parent).cusSearch;
        oldCMND = cus.getCMT();
        showTablePlate();
        setDataCus(cus);
    }

    private void showTablePlate() {
        tableModel.setRowCount(0);
        try {
            ArrayList<String> list = PlateManagement.getPlateByCMT(oldCMND);
            for (int i = 0; i < list.size(); i++) {
                tableModel.addRow(new Object[]{
                    i + 1, list.get(i)
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setDataCus(Customer cus) {
        jTextFieldCMND.setText(cus.getCMT());
        jTextFieldName.setText(cus.getName());
        if (cus.isSex()) {
            jRadioButtonMale.setSelected(true);
        } else {
            jRadioButtonFemale.setSelected(true);
        }
        jTextFieldDOB.setText(new SimpleDateFormat("dd/MM/yyyy").format(cus.getDob()));
        jTextFieldPhone.setText(cus.getPhone());
        jTextFieldAddress.setText(cus.getAddress());
        imageAvatar=cus.getImage();
        Image avatar = cus.getImage().getScaledInstance(jLabelAvatar.getWidth(), jLabelAvatar.getHeight(), ABORT);
        jLabelAvatar.setIcon(new ImageIcon(avatar));
    }
    private boolean checkDataCustomer() {
        String s;
        //check CMND
        s = jTextFieldCMND.getText().trim();
        jTextFieldCMND.setText(s);
        if (!s.matches("(\\d{9})|(\\d{12})")) {
            JOptionPane.showMessageDialog(this, "CMND is not valid");
            jTextFieldCMND.requestFocus();
            return false;
        }
        // check duplicate CMND
        try {
            if (!s.equals(oldCMND)&&CustomerManagement.checkDuplicate(s)) {
                JOptionPane.showMessageDialog(this, "CMND is duplicate");
                jTextFieldCMND.requestFocus();
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // check name
        s = jTextFieldName.getText().trim();
        jTextFieldName.setText(s);
        if (!s.matches("[a-zA-Z\\s]+")) {
            JOptionPane.showMessageDialog(this, "Name is not valid");
            jTextFieldName.requestFocus();
            return false;
        }
        // check DOB
        s = jTextFieldDOB.getText().trim();
        jTextFieldDOB.setText(s);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(s);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this, "DOB is not valid");
            jTextFieldDOB.requestFocus();
            return false;
        }
        // check phone
        s = jTextFieldPhone.getText().trim();
        jTextFieldPhone.setText(s);
        if (!s.matches("\\d{10,11}")) {
            JOptionPane.showMessageDialog(this, "Phone is not valid");
            jTextFieldPhone.requestFocus();
            return false;
        }
        // check image
        if(imageAvatar==null){
            JOptionPane.showMessageDialog(this, "Image is not added");
            jButtonAddImage.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupSex = new javax.swing.ButtonGroup();
        jPopupMenuTable = new javax.swing.JPopupMenu();
        jMenuItemDelete = new javax.swing.JMenuItem();
        jPanel = new javax.swing.JPanel();
        jPanelUpdateCustomer = new javax.swing.JPanel();
        jLabelCMND = new javax.swing.JLabel();
        jTextFieldCMND = new javax.swing.JTextField();
        jLabelName = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jLabelSex = new javax.swing.JLabel();
        jRadioButtonMale = new javax.swing.JRadioButton();
        jRadioButtonFemale = new javax.swing.JRadioButton();
        jLabelDOB = new javax.swing.JLabel();
        jTextFieldDOB = new javax.swing.JTextField();
        jButtonAddImage = new javax.swing.JButton();
        jLabelPhone = new javax.swing.JLabel();
        jTextFieldPhone = new javax.swing.JTextField();
        jLabelAddress = new javax.swing.JLabel();
        jTextFieldAddress = new javax.swing.JTextField();
        jButtonUpdateCustomer = new javax.swing.JButton();
        jButtonDeleteCustomer = new javax.swing.JButton();
        jLabelAvatar = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTablePlate = new javax.swing.JTable();
        jLabelPlateNumber = new javax.swing.JLabel();
        jTextFieldPlateNumber = new javax.swing.JTextField();
        jButtonAddPlate = new javax.swing.JButton();

        jMenuItemDelete.setText("Delete");
        jMenuItemDelete.setToolTipText("");
        jMenuItemDelete.setComponentPopupMenu(jPopupMenuTable);
        jMenuItemDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDeleteActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanelUpdateCustomer.setBackground(new java.awt.Color(255, 255, 204));
        jPanelUpdateCustomer.setBorder(javax.swing.BorderFactory.createTitledBorder("Update Customer"));

        jLabelCMND.setText("CMND");

        jLabelName.setText("Name");

        jLabelSex.setText("Sex");

        buttonGroupSex.add(jRadioButtonMale);
        jRadioButtonMale.setSelected(true);
        jRadioButtonMale.setText("Male");

        buttonGroupSex.add(jRadioButtonFemale);
        jRadioButtonFemale.setText("Female");

        jLabelDOB.setText("DOB");

        jButtonAddImage.setText("Add Image");
        jButtonAddImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddImageActionPerformed(evt);
            }
        });

        jLabelPhone.setText("Phone");

        jLabelAddress.setText("Address");

        jButtonUpdateCustomer.setText("Update");
        jButtonUpdateCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUpdateCustomerActionPerformed(evt);
            }
        });

        jButtonDeleteCustomer.setText("Delete");
        jButtonDeleteCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteCustomerActionPerformed(evt);
            }
        });

        jLabelAvatar.setBackground(new java.awt.Color(0, 0, 255));
        jLabelAvatar.setOpaque(true);

        javax.swing.GroupLayout jPanelUpdateCustomerLayout = new javax.swing.GroupLayout(jPanelUpdateCustomer);
        jPanelUpdateCustomer.setLayout(jPanelUpdateCustomerLayout);
        jPanelUpdateCustomerLayout.setHorizontalGroup(
            jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                        .addComponent(jLabelAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldAddress)
                            .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                                .addComponent(jButtonUpdateCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButtonDeleteCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                            .addComponent(jLabelPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextFieldPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                            .addComponent(jLabelCMND, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jTextFieldCMND, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                            .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabelName, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabelSex, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                                    .addComponent(jRadioButtonMale, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jRadioButtonFemale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(jTextFieldName)))
                        .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                            .addComponent(jLabelDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jTextFieldDOB))))
                .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jLabelAvatar, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelUpdateCustomerLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAddImage)
                        .addGap(44, 44, 44))))
        );
        jPanelUpdateCustomerLayout.setVerticalGroup(
            jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                        .addComponent(jLabelAvatar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(22, 22, 22))
                    .addGroup(jPanelUpdateCustomerLayout.createSequentialGroup()
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCMND)
                            .addComponent(jTextFieldCMND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelName)
                            .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelSex)
                            .addComponent(jRadioButtonMale)
                            .addComponent(jRadioButtonFemale))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelDOB)
                            .addComponent(jTextFieldDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelPhone)
                            .addComponent(jTextFieldPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 15, Short.MAX_VALUE)))
                .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonAddImage)
                    .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelAddress)
                        .addComponent(jTextFieldAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelUpdateCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonUpdateCustomer)
                    .addComponent(jButtonDeleteCustomer))
                .addContainerGap())
        );

        jTablePlate.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "No.", "Plate Number"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTablePlate.setComponentPopupMenu(jPopupMenuTable);
        jTablePlate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTablePlateMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTablePlateMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTablePlate);

        jLabelPlateNumber.setText("Plate Number");

        jButtonAddPlate.setText("Add Plate");
        jButtonAddPlate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddPlateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelLayout = new javax.swing.GroupLayout(jPanel);
        jPanel.setLayout(jPanelLayout);
        jPanelLayout.setHorizontalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabelPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonAddPlate)))
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addComponent(jPanelUpdateCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelLayout.setVerticalGroup(
            jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addGroup(jPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabelPlateNumber, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jTextFieldPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButtonAddPlate)
                        .addGap(18, 18, 18))
                    .addGroup(jPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)))
                .addComponent(jPanelUpdateCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonAddImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddImageActionPerformed
        CameraDialog cameraDialog = new CameraDialog(parent, true);
        cameraDialog.setVisible(true);
        if(cameraDialog.image!=null){
            imageAvatar = cameraDialog.image;
            Image avatar = cameraDialog.image.getScaledInstance(jLabelAvatar.getWidth(), jLabelAvatar.getHeight(), ABORT);
            jLabelAvatar.setIcon(new ImageIcon(avatar));
        }
    }//GEN-LAST:event_jButtonAddImageActionPerformed

    private void jTablePlateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePlateMouseClicked

    }//GEN-LAST:event_jTablePlateMouseClicked

    private void jTablePlateMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTablePlateMousePressed
        int row = jTablePlate.rowAtPoint(evt.getPoint());
        if (row == -1) {
            return;
        }
        jTablePlate.setRowSelectionInterval(row, row);
    }//GEN-LAST:event_jTablePlateMousePressed

    private void jMenuItemDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDeleteActionPerformed
        int row = jTablePlate.getSelectedRow();
        String plateNumber = (String) jTablePlate.getValueAt(row, 1);
        try {
            PlateManagement.removePlate(plateNumber);
            JOptionPane.showMessageDialog(this, "Delete plate successful!");
            showTablePlate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }//GEN-LAST:event_jMenuItemDeleteActionPerformed

    private void jButtonAddPlateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddPlateActionPerformed
        try {
            // check valid plate number
            String plateNumber = jTextFieldPlateNumber.getText().toUpperCase().trim();
            jTextFieldPlateNumber.setText(FormatUtils.formatPlate(plateNumber));
            if (!FormatUtils.isPlate(plateNumber)) {
                JOptionPane.showMessageDialog(this, "Plate number is not valid");
                jTextFieldPlateNumber.requestFocus();
                jTextFieldPlateNumber.setText("");
                return;
            } else if (PlateManagement.checkDuplicate(plateNumber)) {
                JOptionPane.showMessageDialog(this, "Plate number is dupplicate");
                jTextFieldPlateNumber.requestFocus();
                jTextFieldPlateNumber.setText("");
                return;
            }
            int cusID = CustomerManagement.getCustomerIDByCMT(oldCMND);
            PlateManagement.addPlate(new Plate(plateNumber, cusID));
            JOptionPane.showMessageDialog(this, "Add plate successful!");
            showTablePlate();
            jTextFieldPlateNumber.setText("");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_jButtonAddPlateActionPerformed

    private void jButtonUpdateCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUpdateCustomerActionPerformed
        if(checkDataCustomer()){
            Customer c = new Customer();
            c.setCMT(jTextFieldCMND.getText());
            c.setName(jTextFieldName.getText());
            c.setSex(jRadioButtonMale.isSelected());
            try {
                Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(jTextFieldDOB.getText());
                c.setDob(dob);
            } catch (ParseException ex) {
                ex.printStackTrace();
                return;
            }
            c.setPhone(jTextFieldPhone.getText());
            c.setAddress(jTextFieldAddress.getText());
            c.setImage((BufferedImage) imageAvatar);
            try {
                CustomerManagement.updateCustomer(oldCMND, c);
                JOptionPane.showMessageDialog(this, "Update customer successful!");
                this.dispose();
                
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButtonUpdateCustomerActionPerformed

    private void jButtonDeleteCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteCustomerActionPerformed
        if(JOptionPane.showConfirmDialog(this, "Are you sure delete?", "Confirm delete", JOptionPane.YES_NO_OPTION)==0){
            try {
                CustomerManagement.removeCustomer(oldCMND);
                JOptionPane.showMessageDialog(this, "Delete customer successful!");
                this.dispose();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButtonDeleteCustomerActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SearchCustomerDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SearchCustomerDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SearchCustomerDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SearchCustomerDialog.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SearchCustomerDialog dialog = new SearchCustomerDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupSex;
    private javax.swing.JButton jButtonAddImage;
    private javax.swing.JButton jButtonAddPlate;
    private javax.swing.JButton jButtonDeleteCustomer;
    private javax.swing.JButton jButtonUpdateCustomer;
    private javax.swing.JLabel jLabelAddress;
    private javax.swing.JLabel jLabelAvatar;
    private javax.swing.JLabel jLabelCMND;
    private javax.swing.JLabel jLabelDOB;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelPhone;
    private javax.swing.JLabel jLabelPlateNumber;
    private javax.swing.JLabel jLabelSex;
    private javax.swing.JMenuItem jMenuItemDelete;
    private javax.swing.JPanel jPanel;
    private javax.swing.JPanel jPanelUpdateCustomer;
    private javax.swing.JPopupMenu jPopupMenuTable;
    private javax.swing.JRadioButton jRadioButtonFemale;
    private javax.swing.JRadioButton jRadioButtonMale;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTablePlate;
    private javax.swing.JTextField jTextFieldAddress;
    private javax.swing.JTextField jTextFieldCMND;
    private javax.swing.JTextField jTextFieldDOB;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldPhone;
    private javax.swing.JTextField jTextFieldPlateNumber;
    // End of variables declaration//GEN-END:variables
}
