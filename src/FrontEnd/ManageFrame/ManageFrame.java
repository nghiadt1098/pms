/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEnd.ManageFrame;

import BackEnd.Controller.*;
import BackEnd.Model.*;
import BackEnd.Utils.FormatUtils;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author quanbui26111998
 */
public class ManageFrame extends javax.swing.JFrame {

    /**
     * Creates new form ManageFrame
     */
    private String card_id;
    private boolean check; // true is update, false is register
    Webcam webcam;
    BufferedImage imageAvatar = null;
    private String plateNumber = null;
    Customer cusSearch = null;
    ArrayList<Log> listLog = null;
    DefaultTableModel tableLogModel;
    BufferedImage imageLog=null;

    public ManageFrame() {
        webcam = Webcam.getDefault();
        webcam.setViewSize(new Dimension(320, 240));
        initComponents();
        ((WebcamPanel) jPanelQR).setFPSDisplayed(true);
        ((WebcamPanel) jPanelQR).setFillArea(true);
        ((WebcamPanel) jPanelQR).setMirrored(true);
        jTableLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        tableLogModel = (DefaultTableModel) jTableLog.getModel();
    }

    public String formatDate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    private boolean checkDataPlate() {
        String s;
        //check plate number
        s = jTextFieldPlateNumber.getText().toUpperCase().trim();
        plateNumber = s;
        if (!FormatUtils.isPlate(s)) {
            JOptionPane.showMessageDialog(this, "Plate Number is not valid");
            jTextFieldPlateNumber.setText("");
            jTextFieldPlateNumber.requestFocus();
            return false;
        }
        try {
            // check plate number exist
            if (!PlateManagement.checkDuplicate(s)) {
                JOptionPane.showMessageDialog(this, "Plate Number is not exist");
                jTextFieldPlateNumber.setText("");
                jTextFieldPlateNumber.requestFocus();
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        jTextFieldPlateNumber.setText(FormatUtils.formatPlate(s));
        //check expire date
        s = jTextFieldExpireDate.getText().trim();
        jTextFieldExpireDate.setText(s);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        Date expire;
        try {
            expire = format.parse(s);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this, "Expire date is not valid");
            jTextFieldExpireDate.requestFocus();
            return false;
        }
        try {
            if (expire.getTime() <= format.parse(jTextFieldRegisterDate.getText()).getTime()) {
                JOptionPane.showMessageDialog(this, "Expire date must more than register date");
                jTextFieldExpireDate.requestFocus();
                return false;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return true;
    }

    private boolean checkDataCustomer() {
        String s;
        //check CMND
        s = jTextFieldCMND.getText().trim();
        jTextFieldCMND.setText(s);
        if (!s.matches("(\\d{9})|(\\d{12})")) {
            JOptionPane.showMessageDialog(this, "CMND is not valid");
            jTextFieldCMND.requestFocus();
            return false;
        }
        // check duplicate CMND
        try {
            if (CustomerManagement.checkDuplicate(s)) {
                JOptionPane.showMessageDialog(this, "CMND is duplicate");
                jTextFieldCMND.requestFocus();
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // check name
        s = jTextFieldName.getText().trim();
        jTextFieldName.setText(s);
        if (!s.matches("[a-zA-Z\\s]+")) {
            JOptionPane.showMessageDialog(this, "Name is not valid");
            jTextFieldName.requestFocus();
            return false;
        }
        // check DOB
        s = jTextFieldDOB.getText().trim();
        jTextFieldDOB.setText(s);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setLenient(false);
        try {
            format.parse(s);
        } catch (ParseException e) {
            JOptionPane.showMessageDialog(this, "DOB is not valid");
            jTextFieldDOB.requestFocus();
            return false;
        }
        // check phone
        s = jTextFieldPhone.getText().trim();
        jTextFieldPhone.setText(s);
        if (!s.matches("\\d{10,11}")) {
            JOptionPane.showMessageDialog(this, "Phone is not valid");
            jTextFieldPhone.requestFocus();
            return false;
        }
        // check image
        if (imageAvatar == null) {
            JOptionPane.showMessageDialog(this, "Image is not added");
            jButtonAddImage.requestFocus();
            return false;
        }
        return true;
    }

    private void clearInfoCustomer() {
        jTextFieldCMND.setText("");
        jTextFieldName.setText("");
        jRadioButtonMale.setSelected(true);
        jTextFieldDOB.setText("");
        jTextFieldPhone.setText("");
        jTextFieldAddress.setText("");
        jLabelAvatarImage.setIcon(null);
        imageAvatar = null;
    }
    
    private void showLogTable(){
        tableLogModel.setNumRows(0);
        for(int i=listLog.size()-1;i>=0;i--){
            tableLogModel.addRow(new Object[]{
                new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").format(listLog.get(i).getTimeLog()), listLog.get(i).isStatus()? "IN":"OUT"
            });
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupSex = new javax.swing.ButtonGroup();
        jTabbed = new javax.swing.JTabbedPane();
        jPanelPlateManage = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextFieldSearchPlateNumber = new javax.swing.JTextField();
        jButtonSearchPlateNumber = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableLog = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jTextFieldInfoCMND = new javax.swing.JTextField();
        jTextFieldInfoName = new javax.swing.JTextField();
        jTextFieldInfoSex = new javax.swing.JTextField();
        jTextFieldInfoDOB = new javax.swing.JTextField();
        jTextFieldInfoPhone = new javax.swing.JTextField();
        jTextFieldInfoAddress = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabelInfoAvatarImage = new javax.swing.JLabel();
        jButtonClearInfo = new javax.swing.JButton();
        jPanelCardManage = new javax.swing.JPanel();
        jPanelQR = new javax.swing.JPanel();
        jPanelInfo = new javax.swing.JPanel();
        jLabelPlateNumber = new javax.swing.JLabel();
        jTextFieldPlateNumber = new javax.swing.JTextField();
        jLabelRegisterDate = new javax.swing.JLabel();
        jTextFieldRegisterDate = new javax.swing.JTextField();
        jLabelExpireDate = new javax.swing.JLabel();
        jTextFieldExpireDate = new javax.swing.JTextField();
        jButtonScan = new javax.swing.JButton();
        jButtonRegisterUpdate = new javax.swing.JButton();
        jPanelCustomerManage = new javax.swing.JPanel();
        jLabelCMNDSearch = new javax.swing.JLabel();
        jTextFieldSearchCMND = new javax.swing.JTextField();
        jButtonSearchCMND = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelAddCustomer = new javax.swing.JPanel();
        jLabelCMND = new javax.swing.JLabel();
        jTextFieldCMND = new javax.swing.JTextField();
        jLabelName = new javax.swing.JLabel();
        jTextFieldName = new javax.swing.JTextField();
        jLabelSex = new javax.swing.JLabel();
        jRadioButtonMale = new javax.swing.JRadioButton();
        jRadioButtonFemale = new javax.swing.JRadioButton();
        jLabelDOB = new javax.swing.JLabel();
        jTextFieldDOB = new javax.swing.JTextField();
        jButtonAddImage = new javax.swing.JButton();
        jLabelPhone = new javax.swing.JLabel();
        jTextFieldPhone = new javax.swing.JTextField();
        jLabelAddress = new javax.swing.JLabel();
        jTextFieldAddress = new javax.swing.JTextField();
        jButtonClear = new javax.swing.JButton();
        jButtonAdd = new javax.swing.JButton();
        jLabelAvatarImage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jTabbed.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedStateChanged(evt);
            }
        });

        jLabel1.setText("Plate Number");

        jButtonSearchPlateNumber.setText("Search");
        jButtonSearchPlateNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchPlateNumberActionPerformed(evt);
            }
        });

        jTableLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Time", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableLogMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableLog);

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Information Customer");

        jTextFieldInfoCMND.setEditable(false);

        jTextFieldInfoName.setEditable(false);

        jTextFieldInfoSex.setEditable(false);

        jTextFieldInfoDOB.setEditable(false);

        jTextFieldInfoPhone.setEditable(false);

        jTextFieldInfoAddress.setEditable(false);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("CMND");

        jLabelInfoAvatarImage.setBackground(new java.awt.Color(0, 51, 255));
        jLabelInfoAvatarImage.setOpaque(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabelInfoAvatarImage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelInfoAvatarImage, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jButtonClearInfo.setText("Clear");
        jButtonClearInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearInfoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPlateManageLayout = new javax.swing.GroupLayout(jPanelPlateManage);
        jPanelPlateManage.setLayout(jPanelPlateManageLayout);
        jPanelPlateManageLayout.setHorizontalGroup(
            jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldSearchPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                        .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlateManageLayout.createSequentialGroup()
                                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                                        .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldInfoName)
                                            .addComponent(jTextFieldInfoSex, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jTextFieldInfoDOB, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jTextFieldInfoPhone, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jTextFieldInfoAddress)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPlateManageLayout.createSequentialGroup()
                                                .addComponent(jLabel4)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jTextFieldInfoCMND)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(jButtonClearInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(120, 120, 120)))
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                        .addComponent(jButtonSearchPlateNumber)
                        .addContainerGap(189, Short.MAX_VALUE))))
            .addComponent(jSeparator3)
        );
        jPanelPlateManageLayout.setVerticalGroup(
            jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldSearchPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSearchPlateNumber))
                .addGap(4, 4, 4)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelPlateManageLayout.createSequentialGroup()
                                .addGroup(jPanelPlateManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextFieldInfoCMND)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldInfoName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldInfoSex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldInfoDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldInfoPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextFieldInfoAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonClearInfo))
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
        );

        jTabbed.addTab("Plate Management", jPanelPlateManage);

        jPanelQR.setBackground(new java.awt.Color(153, 255, 255));
        jPanelQR.setPreferredSize(new java.awt.Dimension(241, 265));
        jPanelQR = new WebcamPanel(webcam);

        javax.swing.GroupLayout jPanelQRLayout = new javax.swing.GroupLayout(jPanelQR);
        jPanelQR.setLayout(jPanelQRLayout);
        jPanelQRLayout.setHorizontalGroup(
            jPanelQRLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 241, Short.MAX_VALUE)
        );
        jPanelQRLayout.setVerticalGroup(
            jPanelQRLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jLabelPlateNumber.setText("Plate Number");

        jTextFieldPlateNumber.setEditable(false);

        jLabelRegisterDate.setText("Register Date");

        jTextFieldRegisterDate.setEditable(false);

        jLabelExpireDate.setText("Expire Date");

        jTextFieldExpireDate.setEditable(false);

        javax.swing.GroupLayout jPanelInfoLayout = new javax.swing.GroupLayout(jPanelInfo);
        jPanelInfo.setLayout(jPanelInfoLayout);
        jPanelInfoLayout.setHorizontalGroup(
            jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelInfoLayout.createSequentialGroup()
                        .addComponent(jLabelPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldPlateNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE))
                    .addGroup(jPanelInfoLayout.createSequentialGroup()
                        .addComponent(jLabelRegisterDate, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldRegisterDate))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelInfoLayout.createSequentialGroup()
                        .addComponent(jLabelExpireDate, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldExpireDate)))
                .addContainerGap())
        );
        jPanelInfoLayout.setVerticalGroup(
            jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPlateNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelRegisterDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldRegisterDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelExpireDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldExpireDate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonScan.setText("Scan");
        jButtonScan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonScanActionPerformed(evt);
            }
        });

        jButtonRegisterUpdate.setText("Register / Update");
        jButtonRegisterUpdate.setEnabled(false);
        jButtonRegisterUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRegisterUpdateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelCardManageLayout = new javax.swing.GroupLayout(jPanelCardManage);
        jPanelCardManage.setLayout(jPanelCardManageLayout);
        jPanelCardManageLayout.setHorizontalGroup(
            jPanelCardManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCardManageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelQR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelCardManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelCardManageLayout.createSequentialGroup()
                        .addComponent(jButtonScan, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addComponent(jButtonRegisterUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelInfo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelCardManageLayout.setVerticalGroup(
            jPanelCardManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCardManageLayout.createSequentialGroup()
                .addGroup(jPanelCardManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanelQR, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanelCardManageLayout.createSequentialGroup()
                        .addGap(67, 67, 67)
                        .addComponent(jPanelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelCardManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonScan, javax.swing.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                            .addComponent(jButtonRegisterUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(6, 6, 6))
        );

        jTabbed.addTab("Card Management", jPanelCardManage);

        jLabelCMNDSearch.setText("CMND");

        jButtonSearchCMND.setText("Search");
        jButtonSearchCMND.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchCMNDActionPerformed(evt);
            }
        });

        jPanelAddCustomer.setBackground(new java.awt.Color(255, 255, 204));
        jPanelAddCustomer.setBorder(javax.swing.BorderFactory.createTitledBorder("Add Customer"));

        jLabelCMND.setText("CMND");

        jTextFieldCMND.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCMNDActionPerformed(evt);
            }
        });

        jLabelName.setText("Name");

        jLabelSex.setText("Sex");

        buttonGroupSex.add(jRadioButtonMale);
        jRadioButtonMale.setSelected(true);
        jRadioButtonMale.setText("Male");

        buttonGroupSex.add(jRadioButtonFemale);
        jRadioButtonFemale.setText("Female");

        jLabelDOB.setText("DOB");

        jButtonAddImage.setText("Add Image");
        jButtonAddImage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddImageActionPerformed(evt);
            }
        });

        jLabelPhone.setText("Phone");

        jLabelAddress.setText("Address");

        jButtonClear.setText("Clear");
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });

        jButtonAdd.setText("Add");
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });

        jLabelAvatarImage.setBackground(new java.awt.Color(51, 51, 255));
        jLabelAvatarImage.setOpaque(true);

        javax.swing.GroupLayout jPanelAddCustomerLayout = new javax.swing.GroupLayout(jPanelAddCustomer);
        jPanelAddCustomer.setLayout(jPanelAddCustomerLayout);
        jPanelAddCustomerLayout.setHorizontalGroup(
            jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(jButtonClear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(231, 231, 231))
                    .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelCMND, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelName, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelSex, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelDOB, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldName, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jTextFieldCMND, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jTextFieldDOB, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jTextFieldPhone, javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(jTextFieldAddress, javax.swing.GroupLayout.Alignment.CENTER)
                            .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                                .addComponent(jRadioButtonMale, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButtonFemale, javax.swing.GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE)
                                .addGap(41, 41, 41)))
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                                .addGap(79, 79, 79)
                                .addComponent(jButtonAddImage))
                            .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                                .addGap(50, 50, 50)
                                .addComponent(jLabelAvatarImage, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30))))
        );
        jPanelAddCustomerLayout.setVerticalGroup(
            jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelAddCustomerLayout.createSequentialGroup()
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCMND)
                            .addComponent(jTextFieldCMND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelName)
                            .addComponent(jTextFieldName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelSex)
                            .addComponent(jRadioButtonMale)
                            .addComponent(jRadioButtonFemale))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelDOB)
                            .addComponent(jTextFieldDOB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelPhone)
                            .addComponent(jTextFieldPhone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabelAvatarImage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAddress)
                    .addComponent(jTextFieldAddress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAddImage))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelAddCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonClear)
                    .addComponent(jButtonAdd)))
        );

        javax.swing.GroupLayout jPanelCustomerManageLayout = new javax.swing.GroupLayout(jPanelCustomerManage);
        jPanelCustomerManage.setLayout(jPanelCustomerManageLayout);
        jPanelCustomerManageLayout.setHorizontalGroup(
            jPanelCustomerManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCustomerManageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCustomerManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanelCustomerManageLayout.createSequentialGroup()
                        .addComponent(jLabelCMNDSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldSearchCMND, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonSearchCMND)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanelAddCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelCustomerManageLayout.setVerticalGroup(
            jPanelCustomerManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCustomerManageLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCustomerManageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCMNDSearch)
                    .addComponent(jTextFieldSearchCMND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSearchCMND))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelAddCustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );

        jTabbed.addTab("Customer Management", jPanelCustomerManage);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbed)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbed)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonScanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonScanActionPerformed
        jButtonScan.setEnabled(false);
        Thread scanQR = new Thread() {
            @Override
            public void run() {
                do {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Result result = null;
                    BufferedImage image = null;

                    if (webcam.isOpen()) {

                        if ((image = webcam.getImage()) == null) {
                            continue;
                        }

                        LuminanceSource source = new BufferedImageLuminanceSource(image);
                        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

                        try {
                            result = new MultiFormatReader().decode(bitmap);
                        } catch (NotFoundException e) {
                            // fall thru, it means there is no QR code in image
                        }
                    }

                    if (result != null) {
                        card_id = result.getText();
                        try {
                            Card c = CardManagement.getCard(card_id);
                            if (c != null) {
                                check = true;
                                jTextFieldPlateNumber.setText(c.getPlateNumber());
                                //plate_number = c.getPlateNumber();
                                jTextFieldRegisterDate.setText(formatDate(c.getRegisterDate()));
                                jTextFieldExpireDate.setText(formatDate(c.getExpireDate()));
                            } else {
                                check = false;
                                jTextFieldRegisterDate.setText(formatDate(new Date()));
                            }
                            jTextFieldPlateNumber.setEditable(true);
                            jTextFieldExpireDate.setEditable(true);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        jButtonRegisterUpdate.setEnabled(true);
                        jButtonScan.setEnabled(true);
                        break;
                    }

                } while (true);
            }
        };
        scanQR.start();

    }//GEN-LAST:event_jButtonScanActionPerformed

    private void jButtonRegisterUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRegisterUpdateActionPerformed
        if (checkDataPlate()) {
            try {
                Card c = new Card();
                c.setId(card_id);
                c.setPlateNumber(plateNumber);
                /*if (!PlateManagement.checkDuplicate(c.getPlateNumber())) {
                    JOptionPane.showMessageDialog(this, "Plate number is not exist");
                    jTextFieldPlateNumber.requestFocus();
                    return;
                }*/
                c.setRegisterDate(new SimpleDateFormat("dd/MM/yyyy").parse(jTextFieldRegisterDate.getText()));
                c.setExpireDate(new SimpleDateFormat("dd/MM/yyyy").parse(jTextFieldExpireDate.getText()));
                if (check) {
                    CardManagement.updateCard(c);
                    JOptionPane.showMessageDialog(this, "Update successful");
                } else {
                    CardManagement.addCard(c);
                    JOptionPane.showMessageDialog(this, "Add successful");
                }
                jTextFieldPlateNumber.setText("");
                jTextFieldRegisterDate.setText("");
                jTextFieldExpireDate.setText("");
                jTextFieldPlateNumber.setEditable(false);
                jTextFieldExpireDate.setEditable(false);
                jButtonRegisterUpdate.setEnabled(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }//GEN-LAST:event_jButtonRegisterUpdateActionPerformed

    private void jButtonAddImageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddImageActionPerformed
        CameraDialog cameraDialog = new CameraDialog(this, true);
        cameraDialog.setVisible(true);
        if (cameraDialog.image != null) {
            imageAvatar = cameraDialog.image;
            Image avatar = cameraDialog.image.getScaledInstance(jLabelAvatarImage.getWidth(), jLabelAvatarImage.getHeight(), ABORT);
            jLabelAvatarImage.setIcon(new ImageIcon(avatar));
        }
    }//GEN-LAST:event_jButtonAddImageActionPerformed

    private void jTextFieldCMNDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCMNDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldCMNDActionPerformed

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        if (checkDataCustomer()) {
            Customer c = new Customer();
            c.setCMT(jTextFieldCMND.getText());
            c.setName(jTextFieldName.getText());
            c.setSex(jRadioButtonMale.isSelected());
            try {
                Date dob = new SimpleDateFormat("dd/MM/yyyy").parse(jTextFieldDOB.getText());
                c.setDob(dob);
            } catch (ParseException ex) {
                ex.printStackTrace();
                return;
            }
            c.setPhone(jTextFieldPhone.getText());
            c.setAddress(jTextFieldAddress.getText());
            c.setImage((BufferedImage) imageAvatar);
            try {
                CustomerManagement.addCustomer(c);
                JOptionPane.showMessageDialog(this, "Add customer successful!");
                clearInfoCustomer();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }//GEN-LAST:event_jButtonAddActionPerformed

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        clearInfoCustomer();
    }//GEN-LAST:event_jButtonClearActionPerformed

    private void jTabbedStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedStateChanged

    }//GEN-LAST:event_jTabbedStateChanged

    private void jButtonSearchCMNDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchCMNDActionPerformed
        //check CMND
        String s = jTextFieldSearchCMND.getText().trim();
        jTextFieldSearchCMND.setText(s);
        if (!s.matches("(\\d{9})|(\\d{12})")) {
            JOptionPane.showMessageDialog(this, "CMND is not valid");
            jTextFieldSearchCMND.requestFocus();
            return;
        }
        Customer c;
        try {
            c = CustomerManagement.getCustomerByCMT(jTextFieldSearchCMND.getText());
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        if (c == null) {
            JOptionPane.showMessageDialog(this, "CMND is not exist");
            jTextFieldSearchCMND.requestFocus();
            return;
        } else {
            cusSearch = c;
            SearchCustomerDialog searchDia = new SearchCustomerDialog(this, true);
            searchDia.setVisible(true);
        }
    }//GEN-LAST:event_jButtonSearchCMNDActionPerformed

    private void jButtonSearchPlateNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchPlateNumberActionPerformed
        //check valid
        String plateNumberSearch = jTextFieldSearchPlateNumber.getText().toUpperCase().trim();
        jTextFieldSearchPlateNumber.setText(plateNumberSearch);
        if (!FormatUtils.isPlate(plateNumberSearch)) {
            JOptionPane.showMessageDialog(this, "Plate number is not valid");
            jTextFieldSearchPlateNumber.requestFocus();
            return;
        }
        //check exist
        try {
            if (!PlateManagement.checkDuplicate(plateNumberSearch)) {
                JOptionPane.showMessageDialog(this, "Plate Number is not exist");
                jTextFieldSearchPlateNumber.requestFocus();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            Customer c = CustomerManagement.getCustomerByPlate(plateNumberSearch);
            jTextFieldInfoCMND.setText(c.getCMT());
            jTextFieldInfoName.setText(c.getName());
            jTextFieldInfoSex.setText(c.isSex() ? "Male" : "Female");
            jTextFieldInfoDOB.setText(new SimpleDateFormat("dd/MM/yyyy").format(c.getDob()));
            jTextFieldInfoPhone.setText(c.getPhone());
            jTextFieldInfoAddress.setText(c.getAddress());
            Image avatar = c.getImage().getScaledInstance(jLabelInfoAvatarImage.getWidth(), jLabelInfoAvatarImage.getHeight(), ABORT);
            jLabelInfoAvatarImage.setIcon(new ImageIcon(avatar));
            listLog=LogManagement.getLogByPlate(plateNumberSearch);
            showLogTable();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_jButtonSearchPlateNumberActionPerformed

    private void jButtonClearInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearInfoActionPerformed
        jTextFieldSearchPlateNumber.setText("");
        jTextFieldInfoCMND.setText("");
        jTextFieldInfoName.setText("");
        jTextFieldInfoSex.setText("");
        jTextFieldInfoDOB.setText("");
        jTextFieldInfoPhone.setText("");
        jTextFieldInfoAddress.setText("");
        jLabelInfoAvatarImage.setIcon(null);
        tableLogModel.setNumRows(0);
    }//GEN-LAST:event_jButtonClearInfoActionPerformed

    private void jTableLogMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableLogMouseClicked
        if(evt.getClickCount()==2){
            int select = jTableLog.getSelectedRow();
            Log l = listLog.get(listLog.size()-1-select);
            imageLog=l.getImgLog();
            LogImageDialog logImageDia = new LogImageDialog(this, true);
            logImageDia.setVisible(true);
        }
    }//GEN-LAST:event_jTableLogMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManageFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManageFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupSex;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonAddImage;
    private javax.swing.JButton jButtonClear;
    private javax.swing.JButton jButtonClearInfo;
    private javax.swing.JButton jButtonRegisterUpdate;
    private javax.swing.JButton jButtonScan;
    private javax.swing.JButton jButtonSearchCMND;
    private javax.swing.JButton jButtonSearchPlateNumber;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabelAddress;
    private javax.swing.JLabel jLabelAvatarImage;
    private javax.swing.JLabel jLabelCMND;
    private javax.swing.JLabel jLabelCMNDSearch;
    private javax.swing.JLabel jLabelDOB;
    private javax.swing.JLabel jLabelExpireDate;
    private javax.swing.JLabel jLabelInfoAvatarImage;
    private javax.swing.JLabel jLabelName;
    private javax.swing.JLabel jLabelPhone;
    private javax.swing.JLabel jLabelPlateNumber;
    private javax.swing.JLabel jLabelRegisterDate;
    private javax.swing.JLabel jLabelSex;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelAddCustomer;
    private javax.swing.JPanel jPanelCardManage;
    private javax.swing.JPanel jPanelCustomerManage;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelPlateManage;
    private javax.swing.JPanel jPanelQR;
    private javax.swing.JRadioButton jRadioButtonFemale;
    private javax.swing.JRadioButton jRadioButtonMale;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JTabbedPane jTabbed;
    private javax.swing.JTable jTableLog;
    private javax.swing.JTextField jTextFieldAddress;
    private javax.swing.JTextField jTextFieldCMND;
    private javax.swing.JTextField jTextFieldDOB;
    private javax.swing.JTextField jTextFieldExpireDate;
    private javax.swing.JTextField jTextFieldInfoAddress;
    private javax.swing.JTextField jTextFieldInfoCMND;
    private javax.swing.JTextField jTextFieldInfoDOB;
    private javax.swing.JTextField jTextFieldInfoName;
    private javax.swing.JTextField jTextFieldInfoPhone;
    private javax.swing.JTextField jTextFieldInfoSex;
    private javax.swing.JTextField jTextFieldName;
    private javax.swing.JTextField jTextFieldPhone;
    private javax.swing.JTextField jTextFieldPlateNumber;
    private javax.swing.JTextField jTextFieldRegisterDate;
    private javax.swing.JTextField jTextFieldSearchCMND;
    private javax.swing.JTextField jTextFieldSearchPlateNumber;
    // End of variables declaration//GEN-END:variables
}
