package BackEnd.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import BackEnd.Utils.DBContext;

public abstract class Management {
    protected static DBContext context = new DBContext();

    public static boolean checkExist(String tableName, String key, String value) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "SELECT * FROM " + tableName + " WHERE " + key + " = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, value);

        ResultSet rs = ps.executeQuery();
        boolean   ok = rs.next();

        connect.close();

        return ok;
    }

    public static void remove(String tableName, String key, String value) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "DELETE FROM " + tableName + " WHERE " + key + " = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, value);
        ps.execute();
        connect.close();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
