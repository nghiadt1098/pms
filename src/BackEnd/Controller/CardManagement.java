package BackEnd.Controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Date;

import BackEnd.Model.Card;

public class CardManagement extends Management {
    public static void addCard(Card c) throws Exception {
        if (checkDuplicate(c.getId())) {
            throw new Exception("Card is in use");
        }

        Connection        connect = context.getConnection();
        String            query   = "INSERT INTO Card VALUES (?, ?, ?, ?)";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, c.getId());
        ps.setString(2, c.getPlateNumber());
        if (c.getRegisterDate()!=null){
            ps.setDate(3, new java.sql.Date(c.getRegisterDate().getTime()));
        } else {
            ps.setDate(3, null);
        }
        if (c.getExpireDate()!=null){
             ps.setDate(4, new java.sql.Date(c.getExpireDate().getTime()));
        } else {
            ps.setDate(4, null);
        }
       
        ps.execute();
        connect.close();
    }

    public static boolean checkDuplicate(String id) throws Exception {
        return checkExist("Card", "id", id);
    }

    public static void removeCard(String id) throws Exception {
        remove("Card", "id", id);
    }

    public static void updateCard(Card c) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "UPDATE Card SET plate_number = ?, register_date = ?, expire_date = ? WHERE id = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, c.getPlateNumber());
        ps.setDate(2, new java.sql.Date(c.getRegisterDate().getTime()));
        ps.setDate(3, new java.sql.Date(c.getExpireDate().getTime()));
        ps.setString(4, c.getId());
        ps.execute();
        connect.close();
    }

    public static Card getCard(String id) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "SELECT * FROM Card WHERE id = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, id);

        ResultSet rs = ps.executeQuery();
        Card      c  = getData(rs);

        connect.close();

        return c;
    }

    public static Card getData(ResultSet rs) throws Exception {
        if (rs.next()) {
            String id           = rs.getString("id");
            String plateNumber  = rs.getString("plate_number");
            Date   registerDate = rs.getDate("register_date");
            Date   expireDate   = rs.getDate("expire_date");

            return new Card(id, plateNumber, registerDate, expireDate);
        }

        return null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
