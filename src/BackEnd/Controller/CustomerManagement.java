package BackEnd.Controller;

import java.awt.image.BufferedImage;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Date;

import BackEnd.Model.Customer;

import BackEnd.Utils.ImageUtils;

import static BackEnd.Controller.Management.context;

public class CustomerManagement extends Management {
    public static void addCustomer(Customer c) throws Exception {
        if (checkDuplicate(c.getCMT())) {
            throw new Exception("CMND is duplicate");
        }

        Connection        connect = context.getConnection();
        String            query   = "INSERT INTO Customer VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, c.getName());
        ps.setInt(2, c.isSex() ? 1 : 0);
        ps.setDate(3, new java.sql.Date(c.getDob().getTime()));
        ps.setString(4, c.getCMT());
        ps.setString(5, ImageUtils.encoder(c.getImage()));
        ps.setString(6, c.getPhone());
        ps.setString(7, c.getAddress());
        ps.execute();
        connect.close();
    }

    public static boolean checkDuplicate(String CMT) throws Exception {
        return checkExist("Customer", "CMT", CMT);
    }

    public static void removeCustomer(String CMT) throws Exception {
        remove("Customer", "CMT", CMT);
    }

    public static void updateCustomer(String CMT, Customer c) throws Exception {
        if (!c.getCMT().equals(CMT) && checkDuplicate(c.getCMT())) {
            throw new Exception("CMND is duplicate");
        }

        Connection connect = context.getConnection();
        String     query   =
            "UPDATE Customer SET name = ?, sex = ?, dob = ?, CMT = ?, image = ?, phone = ?, address = ? WHERE CMT = ?";
        PreparedStatement ps = connect.prepareStatement(query);

        ps.setString(1, c.getName());
        ps.setInt(2, c.isSex() ? 1 : 0);
        ps.setDate(3, new java.sql.Date(c.getDob().getTime()));
        ps.setString(4, c.getCMT());
        ps.setString(5, ImageUtils.encoder(c.getImage()));
        ps.setString(6, c.getPhone());
        ps.setString(7, c.getAddress());
        ps.setString(8, CMT);
        ps.execute();
        connect.close();
    }

    public static Customer getCustomerByCMT(String CMT) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "SELECT * FROM Customer WHERE CMT = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, CMT);

        ResultSet rs = ps.executeQuery();
        Customer  c  = getData(rs);

        connect.close();

        return c;
    }

    public static Customer getCustomerByPlate(String plateNumber) throws Exception {
        Connection        connect = context.getConnection();
        CallableStatement proc    = connect.prepareCall("{call getCustomerByPlate(?)}");

        proc.setString(1, plateNumber);

        ResultSet rs = proc.executeQuery();
        Customer  c  = getData(rs);

        connect.close();

        return c;
    }

    public static Customer getData(ResultSet rs) throws Exception {
        if (rs.next()) {
            String        name  = rs.getString("name");
            boolean       sex   = (rs.getInt("sex") == 1);
            Date          dob   = rs.getDate("dob");
            String        CMT   = rs.getString("CMT");
            BufferedImage image = ImageUtils.decoder(rs.getString("image"));
            String        phone = rs.getString("phone");
            String        addr  = rs.getString("address");

            return new Customer(name, sex, dob, CMT, image, phone, addr);
        }

        return null;
    }
    
    public static int getCustomerIDByCMT(String CMT) throws Exception{
        Connection        connect = context.getConnection();
        String            query   = "SELECT id FROM Customer WHERE CMT = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, CMT);

        ResultSet rs = ps.executeQuery();
        int id=0;
        if(rs.next()){
            id=rs.getInt("id");
        }

        connect.close();

        return id;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
