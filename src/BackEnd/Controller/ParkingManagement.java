
/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package BackEnd.Controller;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Model.Card;
import BackEnd.Model.Customer;
import BackEnd.Model.Log;
import BackEnd.Model.Plate;

import BackEnd.Utils.Status;
import java.awt.image.BufferedImage;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author MyPC
 */
public class ParkingManagement {
    private ArrayList<Log> logList = new ArrayList<>();
    private Status         status;

    /**
     * Xu ly bien so xe va ma QR tu front end
     * Kiểm tra xe có ở trong garage hay không.
     * Nếu không có => check in
     * Nếu có => Check out
     *
     * @param code
     * @param plateNumber
     * @return
     *         Neu checkin
     *          MATCH neu bien so xe tu the code khop voi plateNumber
     *          NOTMATCH neu bien so xe tu the code khong khop voi Platenumber
     *          NEWCARD Neu Khong thay the trong DB.
     *         Neu checkin
     *          MATCH neu bien so xe tu the code khop voi plateNumber
     *          NOTMATCH neu bien so xe tu the code khong khop voi Platenumber
     */
    
    public Status process(String code, String plateNumber) throws Exception {
            System.out.println("Code "+ code+" Plate "+plateNumber);
            if (LogManagement.getPlateStatus(plateNumber)) {
                Card card = CardManagement.getCard(code);

                if (plateNumber.equals(card.getPlateNumber())) {
                    return this.status=Status.MATCH;
                } else {
                    return this.status=Status.NOTMATCH;
                }
            } else {
                Card card = CardManagement.getCard(code);

                if (card == null) {
                    return this.status=Status.NEWCARD;
                }

                if (plateNumber.equals(card.getPlateNumber())) {
                    return this.status=Status.MATCH;
                } else {
                    return this.status=Status.NOTMATCH;
                }
            }
    }
    
    ;
    public static Log getLatestLog() throws Exception{
        if (LogManagement.getLog(1).size()==0){
            return null;
        }
        return LogManagement.getLog(1).get(0);
    }
    /**
     * 
     * 
     * @param code
     * @param plateNumber
     */
    public void confirm(String code, String plateNumber ,BufferedImage img) throws Exception {
         
        if (LogManagement.getPlateStatus(plateNumber)) {
            switch(status){
                case NEWCARD:
                    
                    break;
                case MATCH:
                    Card c=CardManagement.getCard(code);
                    if(c.getRegisterDate()==null){
                        CardManagement.removeCard(code);
                    }
                    LogManagement.addLog(new Log(plateNumber, new Date(), false, img)); 
                    break;
                case NOTMATCH:
                    break;
                }
            } else {
                
               switch(status){
                case NEWCARD:
                    Card c =new Card();
                    c.setId(code);
                    c.setPlateNumber(plateNumber);
                    CardManagement.addCard(c);
                    break;
                case MATCH:
                     LogManagement.addLog(new Log(plateNumber, new Date(), true, img)); 
                    break;
                case NOTMATCH:
                    break;
                }
               
            }
       
    }

    public ArrayList<Log> getLogs(int n) throws Exception {
        return LogManagement.getLog(n);
    }

    /**
     *
     * @param code truyen vao code trong QR
     * @return thong tin ve card
     * @throws java.lang.Exception
     */
    public Card getCardFromQR(String code) throws Exception {
        return CardManagement.getCard(code);
    }

    /**
     *
     * @param plate truyen vao bien so
     * @return khach hang so huu bien so
     * @throws java.lang.Exception
     */
    public Customer getUserFromPlate(String plate) throws Exception {
        return CustomerManagement.getCustomerByPlate(plate);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
