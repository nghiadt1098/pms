package BackEnd.Controller;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;

import BackEnd.Model.Plate;

public class PlateManagement extends Management {
    public static void addPlate(Plate p) throws Exception {
        if (checkDuplicate(p.getPlateNumber())) {
            throw new Exception("Plate number is duplicate");
        }

        if (checkForeignKey(p.getCustomerID())) {
            throw new Exception("Customer ID doesn't exist");
        }

        Connection        connect = context.getConnection();
        String            query   = "INSERT INTO Plate VALUES (?, ?)";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, p.getPlateNumber());
        ps.setInt(2, p.getCustomerID());
        ps.execute();
        connect.close();
    }

    public static boolean checkDuplicate(String plateNumber) throws Exception {
        return checkExist("Plate", "plate_number", plateNumber);
    }

    public static boolean checkForeignKey(int id) throws Exception {
        return !checkExist("Customer", "id", "" + id);
    }

    public static void removePlate(String plateNumber) throws Exception {
        remove("Plate", "plate_number", plateNumber);
    }

    public static ArrayList<String> getPlateByCMT(String CMT) throws Exception {
        ArrayList<String> arr     = new ArrayList<>();
        Connection        connect = context.getConnection();
        CallableStatement proc    = connect.prepareCall("{call getPlateByCMT(?)}");

        proc.setString(1, CMT);

        ResultSet rs = proc.executeQuery();

        while (rs.next()) {
            arr.add(rs.getString("plate_number"));
        }

        connect.close();

        return arr;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
