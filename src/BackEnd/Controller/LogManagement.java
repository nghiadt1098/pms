package BackEnd.Controller;

//~--- non-JDK imports --------------------------------------------------------

import BackEnd.Model.Log;

import BackEnd.Utils.ImageUtils;

import static BackEnd.Controller.Management.context;

//~--- JDK imports ------------------------------------------------------------

import java.awt.image.BufferedImage;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Date;

public class LogManagement extends Management {
    public static void addLog(Log l) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "INSERT INTO Log VALUES (?, ?, ?, ?)";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, l.getPlateNumber());
        ps.setTimestamp(2, new java.sql.Timestamp(l.getTimeLog().getTime()));
        ps.setInt(3, l.isStatus()
                     ? 1
                     : 0);
        ps.setString(4, ImageUtils.encoder(l.getImgLog()));
        ps.execute();
        connect.close();
    }

    public static ArrayList<Log> getData(ResultSet rs) throws Exception {
        ArrayList<Log> arr = new ArrayList<>();

        while (rs.next()) {
            String        plateNumber = rs.getString("plate_number");
            Date          timeLog     = rs.getTimestamp("time_log");
            boolean       status      = (rs.getInt("status") == 1);
            BufferedImage imgLog      = ImageUtils.decoder(rs.getString("image_log"));

            arr.add(new Log(plateNumber, timeLog, status, imgLog));
        }

        return arr;
    }

    public static ArrayList<Log> getLogByCMT(String CMT) throws Exception {
        Connection        connect = context.getConnection();
        CallableStatement proc    = connect.prepareCall("{call getLogByCMT(?)}");

        proc.setString(1, CMT);

        ResultSet      rs  = proc.executeQuery();
        ArrayList<Log> arr = getData(rs);

        connect.close();

        return arr;
    }

    public static ArrayList<Log> getLog(int maxRow) throws SQLException, Exception {
        ArrayList<Log>    list    = new ArrayList<>();
        Connection        connect = context.getConnection();
        String            querry  = "SELECT TOP "+maxRow+" * FROM Log ORDER BY ID DESC";
        PreparedStatement ps      = connect.prepareStatement(querry);


        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            Log           log         = new Log();
            String        plateNumber = rs.getString("plate_number");
            Date          timeLog     = rs.getTimestamp("time_log");
            boolean       status      = (rs.getInt("status") == 1);
            BufferedImage imgLog      = ImageUtils.decoder(rs.getString("image_log"));
            log.setPlateNumber(plateNumber);
            log.setTimeLog(timeLog);
            log.setStatus(status);
            log.setImgLog(imgLog);
            list.add(0, log);
        }
        return list;
    }

    public static ArrayList<Log> getLogByPlate(String plateNumer) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "SELECT * FROM Log WHERE plate_number = ?";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, plateNumer);

        ResultSet      rs  = ps.executeQuery();
        ArrayList<Log> arr = getData(rs);

        connect.close();

        return arr;
    }

    public static boolean getPlateStatus(String plateNumber) throws Exception {
        Connection        connect = context.getConnection();
        String            query   = "SELECT TOP 1 * FROM Log WHERE plate_number = ? ORDER BY time_log DESC";
        PreparedStatement ps      = connect.prepareStatement(query);

        ps.setString(1, plateNumber);

        ResultSet rs = ps.executeQuery();

        if (!rs.next()) {
            throw new Exception("This plate is non-exist");
        }

        int i = rs.getInt("status");

        connect.close();

        return (i == 1);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
