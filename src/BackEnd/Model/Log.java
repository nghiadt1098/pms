package BackEnd.Model;

import java.awt.image.BufferedImage;

import java.util.Date;

public class Log {
    private String        plateNumber;
    private Date          timeLog;
    private boolean       status;
    private BufferedImage imgLog;

    public Log() {}

    public Log(String plateNumber, Date timeLog, boolean status, BufferedImage imgLog) {
        this.plateNumber = plateNumber;
        this.timeLog     = timeLog;
        this.status      = status;
        this.imgLog      = imgLog;
    }

    public BufferedImage getImgLog() {
        return imgLog;
    }

    public void setImgLog(BufferedImage imgLog) {
        this.imgLog = imgLog;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getTimeLog() {
        return timeLog;
    }

    public void setTimeLog(Date timeLog) {
        this.timeLog = timeLog;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
