package BackEnd.Model;

public class Plate {
    private String plateNumber;
    private int    customerID;

    public Plate() {}

    public Plate(String plateNumber, int customerID) {
        this.plateNumber = plateNumber;
        this.customerID  = customerID;
    }

    public Plate(String plateNumber) {
        this.plateNumber = plateNumber;
    }
    
    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
