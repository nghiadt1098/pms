package BackEnd.Model;

import java.util.Date;

public class Card {
    private String id;
    private String plateNumber;
    private Date   registerDate;
    private Date   expireDate;

    public Card() {}

    public Card(String id, String plateNumber, Date registerDate, Date expireDate) {
        this.id           = id;
        this.plateNumber  = plateNumber;
        this.registerDate = registerDate;
        this.expireDate   = expireDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
