package BackEnd.Model;

import java.awt.image.BufferedImage;

import java.util.Date;

public class Customer {
    private String        name;
    private boolean       sex;
    private Date          dob;
    private String        CMT;
    private BufferedImage image;
    private String        phone;
    private String        address;

    public Customer() {}

    public Customer(String name, boolean sex, Date dob, String CMT, BufferedImage image, String phone, String address) {
        this.name    = name;
        this.sex     = sex;
        this.dob     = dob;
        this.CMT     = CMT;
        this.image   = image;
        this.phone   = phone;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCMT() {
        return CMT;
    }

    public void setCMT(String CMT) {
        this.CMT = CMT;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
