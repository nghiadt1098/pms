package BackEnd.Utils;

import java.awt.image.BufferedImage;

import java.io.File;

import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;

import com.github.sarxos.webcam.util.jh.JHGrayFilter;

public class ANPRUtils {
    public static ArrayList<String> getANPRfromImage(BufferedImage img) throws Exception {
        ArrayList<String> str = new ArrayList<>();

        ImageIO.write(new JHGrayFilter().filter(img, null), "PNG", new File("img.png"));

        Runtime  rt  = Runtime.getRuntime();
        String[] cmd = { "anpr.exe", "img.png" };
        Process  p   = rt.exec(cmd);
        Scanner  sc  = new Scanner(p.getInputStream());

        while (sc.hasNext()) {
            str.add(sc.nextLine());
        }

        return str;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
