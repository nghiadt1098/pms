package BackEnd.Utils;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

public class QRUtils {
    public static BufferedImage encodeQR(String str) throws IOException {
        File          f   = QRCode.from(str).to(ImageType.PNG).withSize(256, 256).file();
        BufferedImage img = ImageIO.read(f);

        return img;
    }

    public static String getQRFromImage(BufferedImage img) throws Exception {
        LuminanceSource src = new BufferedImageLuminanceSource(img);
        BinaryBitmap    bmp = new BinaryBitmap(new HybridBinarizer(src));
        Result          res = new MultiFormatReader().decode(bmp);

        return res.getText();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
