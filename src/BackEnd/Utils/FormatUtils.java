package BackEnd.Utils;

public class FormatUtils {
    public static String formatCMT(String s) {
        if (s.length() == 9) {
            return s.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1 $2 $3");
        } else {
            return s.replaceFirst("(\\d{3})(\\d{3})(\\d{3})(\\d+)", "$1 $2 $3 $4");
        }
    }

    public static String formatPhone(String s) {
        return s.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1)$2-$3");
    }

    public static String formatPlate(String s) {
        return s.replaceFirst("(\\d{2})(\\w{2})(\\d+)", "$1-$2 $3");
    }

    public static boolean isPlate(String s) {
        return s.matches("(\\d{2})(\\w{2})(\\d{4,5})");
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
