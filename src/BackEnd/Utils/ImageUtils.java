package BackEnd.Utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.net.URL;

import java.util.Base64;

import javax.imageio.ImageIO;

import javax.swing.ImageIcon;

public class ImageUtils {
    public static BufferedImage decoder(String imgB64) throws Exception {
        byte[]               arr = Base64.getDecoder().decode(imgB64);
        ByteArrayInputStream bis = new ByteArrayInputStream(arr);
        BufferedImage        img = ImageIO.read(bis);

        return img;
    }

    public static String encoder(BufferedImage img) throws Exception {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        ImageIO.write(img, "PNG", bos);

        byte[] arr = bos.toByteArray();

        return Base64.getEncoder().encodeToString(arr);
    }

    public static String encoder(String path) throws Exception {
        File            file = new File(path);
        FileInputStream fImg = new FileInputStream(file);
        byte[]          arr  = new byte[(int) file.length()];

        fImg.read(arr);

        return Base64.getEncoder().encodeToString(arr);
    }

    public static BufferedImage readFromFile(String path) throws Exception {
        return ImageIO.read(new File(path));
    }

    public static void saveToFile(String path, BufferedImage img) throws Exception {
        ImageIO.write(img, "PNG", new File(path));
    }

    public static ImageIcon getScaledIco(URL url, int width, int height) throws Exception, IOException {
        Image     dimg      = ImageIO.read(url).getScaledInstance(width, height, Image.SCALE_SMOOTH);
        ImageIcon imageIcon = new ImageIcon(dimg);

        return imageIcon;
    }
    public static BufferedImage toBufferedImage(Image img)
{
    if (img instanceof BufferedImage)
    {
        return (BufferedImage) img;
    }

    // Create a buffered image with transparency
    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

    // Draw the image on to the buffered image
    Graphics2D bGr = bimage.createGraphics();
    bGr.drawImage(img, 0, 0, null);
    bGr.dispose();

    // Return the buffered image
    return bimage;
}
}


//~ Formatted by Jindent --- http://www.jindent.com
