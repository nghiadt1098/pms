package BackEnd.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;

import java.util.Properties;

public class DBContext {
    private String serverName;
    private String dbName;
    private String portNumber;
    private String userID;
    private String password;

    public DBContext() {
        File configFile = new File("config.properties");

        try {
            FileReader reader = new FileReader(configFile);
            Properties props  = new Properties();

            props.load(reader);
            this.serverName = props.getProperty("host");
            this.dbName     = props.getProperty("db");
            this.portNumber = props.getProperty("port");
            this.userID     = props.getProperty("user");
            this.password   = props.getProperty("pass");
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
    }

    @Override
    public String toString() {
        return "DBContext{" + "serverName=" + serverName + ", dbName=" + dbName + ", portNumber=" + portNumber
               + ", userID=" + userID + ", password=" + password + '}';
    }

    public Connection getConnection() throws Exception {
        String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + ";databaseName=" + dbName;

        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        return DriverManager.getConnection(url, userID, password);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
