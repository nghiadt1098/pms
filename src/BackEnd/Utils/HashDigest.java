package BackEnd.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashDigest {
    public static String getMd5(String str) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");

        md.update(str.getBytes());

        byte[]        arr = md.digest();
        StringBuilder sb  = new StringBuilder();

        for (byte b : arr) {
            sb.append(String.format("%02x", b & 0xff));
        }

        return sb.toString();
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
