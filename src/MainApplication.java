//~--- non-JDK imports --------------------------------------------------------

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
import BackEnd.Utils.ImageUtils;

import FrontEnd.MainFrame.MainFrame;

import FrontEnd.ManageFrame.ManageFrame;

import javafx.application.Application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;

import javafx.stage.Stage;

//~--- JDK imports ------------------------------------------------------------

import com.sun.imageio.plugins.common.ImageUtil;

import java.io.IOException;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;

import javax.imageio.ImageIO;

import javax.swing.JFrame;

/**
 *
 * @author MyPC
 */
public class MainApplication extends Application {
    public static Stage pStage;
    private Parent       root;
    public static Scene scene;
    
    
    @Override
    public void start(Stage primaryStage) {
        try {
            setPrimaryStage(primaryStage);
            pStage = primaryStage;

            Parent root = FXMLLoader.load(getClass().getResource("MainApp.fxml"));

            this.root = root;
            
            Scene scene = new Scene(root, 970, 656);
            this.scene=scene;
            primaryStage.setTitle("Parking Management System");
            primaryStage.getIcons().add(
                new Image(MainApplication.class.getResourceAsStream("Resources/Icon/icons8_Garage_500px_3.png")));
            scene.getStylesheets().add(getClass().getResource("mainapp.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.show();
            primaryStage.setResizable(false);
            
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
     @FXML
    public void initialize() {
        System.out.println("second");
    }
    public static Stage getPrimaryStage() {
        return pStage;
    }

    private void setPrimaryStage(Stage pStage) {
        MainApplication.pStage = pStage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
